#!/bin/sh

#==============================================================================
# git-push-to-gitlab
# File ID: 4f906152-875d-11ef-9cd0-754a1be59913
#
# [Description]
#
# Author: Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later.
#==============================================================================

progname=git-push-to-gitlab
VERSION=0.0.0

T_GREEN=$(tput setaf 2)
T_RESET=$(tput sgr0)

opt_help=0
opt_quiet=0
opt_verbose=0

while test -n "$1"; do
	case "$1" in
	-h|--help) opt_help=1; shift ;;
	-q|--quiet) opt_quiet=$(($opt_quiet + 1)); shift ;;
	-v|--verbose) opt_verbose=$(($opt_verbose + 1)); shift ;;
	--version) echo $progname $VERSION; exit 0 ;;
	--) shift; break ;;
	*)
		if printf '%s\n' "$1" | grep -q ^-; then
			echo "$progname: $1: Unknown option" >&2
			exit 1
		else
			break
		fi
	break ;;
	esac
done
opt_verbose=$(($opt_verbose - $opt_quiet))

if test "$opt_help" = "1"; then
	test $opt_verbose -gt 0 && { echo; echo $progname $VERSION; }
	cat <<END

Usage: $progname [options]

Options:

  -h, --help
    Show this help.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  --version
    Print version information.

END
	exit 0
fi

if git branch | grep 'tmp-gitlab'; then
	echo $progname: tmp-gitlab branch already exists >&2
	exit 1
fi
git fetch gitlab
for rev in $(git sht --reverse gitlab/master..master); do
	echo
	GIT_PAGER=cat git log -1 --format="$T_GREEN%h$T_RESET %s" $rev \
	&& echo git branch tmp-gitlab $rev \
	&& git branch tmp-gitlab $rev \
	&& echo git push -f gitlab tmp-gitlab:master \
	&& git push -f gitlab tmp-gitlab:master \
	&& echo git branch -D tmp-gitlab \
	&& git branch -D tmp-gitlab \
	&& if git sht gitlab/master..master | grep -q ^; then
		printf '%s sleep 60...' $(date +%H:%M:%S)
		sleep 60
		echo ok
	   fi \
	|| exit 1
done
git pa -s
git push gitlab

# vim: set ts=8 sw=8 sts=8 noet fo+=w tw=79 fenc=UTF-8 :
