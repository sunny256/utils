% STDfilenameDTS
% File ID: STDuuidDTS
% Author: Øyvind A. Holm <sunny@sunbase.org>

mbar = \drummode { ss4 ss ss ss }

metronome = {

% Begin

  \initTempo
  \time 4/4

  \mbar
  |

% End

}

% vim: set tw=0 :
